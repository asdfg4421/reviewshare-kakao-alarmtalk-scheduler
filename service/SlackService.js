const slackNode = require('slack-node');
const webHookUri = "https://hooks.slack.com/services/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";
const slack = new slackNode();
slack.setWebhook(webHookUri);

const channelName = "#aws_notification";
const userName = "ReviewShareBot";

const sendSlack = (message) => {

    return new Promise((resolve, reject) => {
        return slack.webhook({
            channel: channelName,
            username: userName,
            text: "",
            attachments: [
                {
                    "title": "스케쥴러 유저 알림 전송.",
                    "text": message,
                    "color": "#3AA3E3"
                }
            ]
        }, (err, respone) => {
            return resolve();
        });
    });
}

module.exports = {
    sendSlack,
};