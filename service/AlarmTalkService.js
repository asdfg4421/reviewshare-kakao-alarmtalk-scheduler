const request = require('request-promise-native');

const sendAlarmTalk = async (message, template, phoneNumber) => {

    try {

        const body = {
            account: `${process.env.BIZBBURIO_ACCOUNT}`,
            refkey: `${process.env.BIZBBURIO_ACCOUNT}` + '_' + Date.now(), // 임의의 고유값
            type: 'at',
            from: '15888781',
            to: phoneNumber,
            content: {
                at: {
                    message: message,
                    senderkey: `${process.env.BIZBBURIO_SENDERKEY}`,
                    templatecode: template,
                    button: [{
                        name: '리뷰쉐어 바로가기',
                        type: 'WL',
                        url_pc: 'https://reviewshare.io',
                        url_mobile: 'https://reviewshare.io'
                    }]
                }
            },
            resend: 'lms',
            recontent: {
                lms: {
                    message: message
                }
            }
        };

        console.log(message);

        const uri = `${process.env.BIZBBURIO_URL}/v1/message`;

        const options = {
            method: 'POST',
            uri: uri,
            timeout: 10000,
            rejectUnauthorized: false,
            header: {
                'Content-type': 'application/json'
            },
            body: body,
            json: true
        };

        const result = await request(options);

        console.log(result);

    } catch (err) {
        console.log(err);
        return null;
    };
}

module.exports = {
    sendAlarmTalk: sendAlarmTalk
}
