const sendgridMail = require('@sendgrid/mail');
const fs = require('fs');

sendgridMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendMail = async (to, fileName, templateData) => {

    try {

        const template = fs.readFileSync(`./EmailTemplate/${fileName}`, 'utf8');

        const compileMailTemplate = _.template(template)(templateData);

        const emailDatas = compileMailTemplate.match(/^([^\n]*)\n[^\n]*\n((.|\n|\r)*)(html:)((.|\n|\r)*)/m);

        const msg = {
            to: to,
            from: process.env.EMAIL_FROM,
            subject: emailDatas[1],
            html: emailDatas[5]
        };

        return sendgridMail.send(msg);

    } catch (err) {
        console.log(err);
        return null;
    }
}

module.exports = {
    sendMail: sendMail
}
