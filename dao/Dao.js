const findChosenUser = async () => {

    const todayStartAt = Moment().startOf('day');
    const todayEndAt = Moment().endOf('day');

    const projects = await Campaign.find({
        isDeleted: false,
        submitStartAt: { $gte: todayStartAt, $lte: todayEndAt },
        status: 'Running'
    }, { _id: 1 }).lean();

    const projectIds = projects.map(project => project._id);

    const requests = await CampaignRequest.find({
        campaign: { $in: projectIds },
        isDeleted: false,
        isChosen: true,
        isCancel: { $ne: true }
    }, { _id: 1, owner: 1, campaign: 1 })
        .populate([{
            path: 'owner',
            model: 'User',
            select: 'phoneNumber name'
        }, {
            path: 'campaign',
            model: 'Campaign',
            select: 'title category sns otherSns submitStartAt submitEndAt phoneNumber address addressDetail itemName visitDesc'
        }]).lean();

    return requests;
}

const findUnsubmitUser7days = async () => {

    // 오늘을 포함해서 7일 남았다. ==> 6일을 더해준다.
    const startAt7Days = Moment().add(6, 'day').startOf('day');
    const endAt7Days = Moment().add(6, 'day').endOf('day');

    const requests = await CampaignRequest.find({
        submitEndAt: { $gte: startAt7Days, $lte: endAt7Days },
        isChosen: true,
        isSubmitted: false,
        isCancel: { $ne: true },
        isDeleted: false
    }, { _id: 1, owner: 1, campaign: 1 })
        .populate([{
            path: 'owner',
            model: 'User',
            select: 'phoneNumber name'
        }, {
            path: 'campaign',
            model: 'Campaign',
            select: 'title'
        }]).lean();

    return requests;
}

const findUnsubmitUser1day = async () => {

    // 오늘을 빼고 하루 남았다. ==> 하루만 더해준다.
    const startAt1Day = Moment().add(1, 'day').startOf('day');
    const endAt1Day = Moment().add(1, 'day').endOf('day');

    const requests = await CampaignRequest.find({
        submitEndAt: { $gte: startAt1Day, $lte: endAt1Day },
        isChosen: true,
        isSubmitted: false,
        isCancel: { $ne: true },
        isDeleted: false
    }, { _id: 1, owner: 1, campaign: 1 })
        .populate([{
            path: 'owner',
            model: 'User',
            select: 'phoneNumber name'
        }, {
            path: 'campaign',
            model: 'Campaign',
            select: 'title'
        }]).lean();

    return requests;
}

const findInstagramReviews6months = async () => {

    let startDate = Moment('2019-07-16').startOf('day');
    const now = Moment();

    if (now.diff(startDate, 'month') >= 6) {
        startDate = Moment();
        startDate = startDate.subtract(6, 'month').startOf('day');
    }

    const campaignRequestQuery = {
        isChosen: true,
        isSubmitted: true,
        isCancel: { $ne: true },
        url: { $exists: true },
        submittedAt: { $gte: startDate.toDate(), $lte: now.toDate() },
        isDeleted: false
    };

    const campaignLookup = {
        from: 'campaigns',
        localField: 'campaign',
        foreignField: '_id',
        as: 'campaign'
    };

    const userLookup = {
        from: 'users',
        localField: 'owner',
        foreignField: '_id',
        as: 'owner'
    };

    const filter = {
        url: 1,
        'campaign.title': 1,
        'campaign._id': 1,
        'owner.phoneNumber': 1,
        'owner.name': 1
    };

    const campaignRequests = await CampaignRequest.aggregate(
        { $match: campaignRequestQuery },
        { $lookup: campaignLookup },
        { $unwind: '$campaign' },
        { $match: { 'campaign.sns': 'Instagram', 'campaign.isDeleted': false } },
        { $lookup: userLookup },
        { $unwind: { path: '$owner', "preserveNullAndEmptyArrays": true } },
        { $project: filter }
    );

    return campaignRequests;
}

const findChoiceDayAdvertiser = async () => {

    const todayEndAt = Moment().endOf('day');
    const todayStartAt = Moment().startOf('day');

    const query = {
        isDeleted: false,
        status: 'Running',
        choiseDay: { $gte: todayStartAt, $lte: todayEndAt }
    };

    const projects = await Campaign.find(query, {
        title: 1, owner: 1
    }).populate({
        path: 'owner',
        model: 'User',
        select: 'name phoneNumber'
    }).lean();

    return projects;
}

const findTodayCompleteProjects = async () => {

    const submitEndDateStart = Moment().startOf('day').subtract(1, 'day');
    const submitEndDateEnd = Moment().endOf('day').subtract(1, 'day');

    const query = {
        isDeleted: false,
        status: { $nin: ['Reviewing', 'Rejected'] },
        submitEndAt: { $gte: submitEndDateStart, $lte: submitEndDateEnd }
    };

    const projects = await Campaign.find(query, {
        owner: 1, title: 1, sns: 1, otherSns: 1, requestUsers: 1
    }).populate({
        path: 'owner',
        model: 'User',
        select: 'name phoneNumber'
    }).lean();

    return projects;
}

const findWelcomePointExpireBefore3DaysAdvertiser = async () => {

    const sumUsedPointValues = (array) => {
        let sum = 0;
        for (let payment of array) {
            sum += payment.usedPoints;
        }
        return sum;
    }

    const expiredStart = Moment().startOf('day').subtract(11, 'day');
    const expiredEnd = Moment().endOf('day').subtract(11, 'day');

    const query = {
        createdAt: {
            $gte: expiredStart,
            $lte: expiredEnd
        },
        category: "Seller",
        isDeleted: false
    };

    const advertisers = await User.find(query,
        { _id: 1, phoneNumber: 1, createdAt: 1, name: 1 })
        .lean();

    const pointRemainAdvertisers = [];

    await Promise.all(advertisers.map(async advertiser => {

        const query = {
            owner: advertiser._id,
            status: { $in: ["paid", "ready"] },
            usedPoints: { $exists: true },
            createdAt: {
                $gte: Moment(advertiser.createdAt),
                $lte: Moment()
            },
            isDeleted: false
        };

        const payments = await Payment.find(query).lean();
        const usedPointSum = sumUsedPointValues(payments);

        if (usedPointSum < 10000) {
            return pointRemainAdvertisers.push(advertiser);
        } else {
            return null;
        }
    }));

    return pointRemainAdvertisers;
}

const getChosenUsersCount = async (project) => {

    return await CampaignRequest.find({
        campaign: project._id,
        isChosen: true,
        isDeleted : false
    }, { isSubmitted: 1 }).lean();
}

module.exports = {
    findChosenUser,
    findUnsubmitUser7days,
    findUnsubmitUser1day,
    findInstagramReviews6months,
    findChoiceDayAdvertiser,
    findTodayCompleteProjects,
    findWelcomePointExpireBefore3DaysAdvertiser,
    getChosenUsersCount
}