const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    _id : {type : Number},
    name: { type: String },
    nickname: { type: String },
    phoneNumber: { type: String },
    email: { type: String },
    isDeleted: { type: Boolean }
});

module.exports = mongoose.model('User', schema);