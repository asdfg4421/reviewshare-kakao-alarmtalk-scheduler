const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    _id: { type: Number },
    title: { type: String },
    isDeleted: { type: Boolean },
    choiseDay: { type: Date },
    submitStartAt: { type: Date },
    submitEndAt: { type: Date },
    phoneNumber: { type: String },
    sns: { type: String },
    otherSns: { type: String },
    visitDesc: { type: String },
    address: { type: mongoose.Schema.Types.Mixed },
    status: { type: String, enum: ['Rejected', 'Reviewing', 'Running', 'Complete'] },
    requestUsers : {type : [Number], ref : 'User'}
});

module.exports = mongoose.model('Campaign', schema);