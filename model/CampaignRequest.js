const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    _id: { type: Number },
    campaign: { type: Number, ref: 'Campaign' },
    isChosen: { type: Boolean },
    isDeleted: { type: Boolean },
    owner: { type: Number, ref: 'User' },
    isCancel: { type: Boolean },
    isSubmitted: { type: Boolean },
    choiseDay: { type: Date },
    submitEndAt: { type: Date },
    submittedAt: { type: Date },
    url: { type: String }
});

module.exports = mongoose.model('CampaignRequest', schema);