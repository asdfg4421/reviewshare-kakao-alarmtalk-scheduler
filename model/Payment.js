const mongoose = require('mongoose');

const schema = new mongoose.Schema({

    // point충전 -> 건당결제로 바뀌면서 추가.
    // 기본 필드값을 함부로 지울수 없음. admin에서 조회시 오류가 생길 수 있으므로.

    depositEndAt: { type: Date }, // 무통장 입금시 입금 기한.
    country: { type: String, default: 'KR' }, // 어느 나라에서 결제한건가.
    // currency : {type: String, default: 'KRW'}, // 결제 통화가 무엇인가. 일단 dollor로 갈거 같다.
    // Properties
    amount: { type: Number },//충전포인트(원랜 충전금액이엇으나 3차개발이후 부가세가 추가되면서 바꿈)
    points: { type: Number }, //프로젝트 생성할때 제공 포인트를 넣어줬으나 이니시스로 결제바꾸면서 안 쓰게 됨/
    amountReal: { type: Number },//부가세붙여서 실제 결제한 금액
    method: { type: String, enum: ['Charge', 'Use'] }, //2018.12 건별 결제로 바뀌면서 사용하지 않음
    usedPoints: { type: Number }, //포인트결제와 이니시스 결제 둘 다 가능하게 되면서 추가됨.
    accountName: { type: String },
    bankAccount: { type: String },
    bankName: { type: String },
    accountHolder: { type: String },

    moid: { type: String }, //이니시스로 변경하면서 추가. 캠페인과의 연결을 위해 추가.

    //iamport의 pay_method
    category: { type: String, enum: ['card', 'trans', 'deposit', 'point', 'refund', 'event'] },

    /**
     * 어플리캣에서 파악 할수 있는 에러 메세지를 담는 곳
     * 만약에 파악할수 없으면 PG사한테 책임전가
     **/
    message: { type: String },

    card_name: { type: String },
    currency: { type: String },
    custom_data: { type: String },
    fail_reason: { type: String },
    failed_at: { type: String },
    cancel_amount: { type: String },
    cancel_reason: { type: String },
    cancel_history: [{ type: String }],
    cancel_receipt_urls: [{ type: String }],
    buyer_tel: { type: String },
    buyer_postcode: { type: String },
    buyer_name: { type: String },
    buyer_email: { type: String },
    bank_name: { type: String },
    apply_num: { type: String },
    receipt_url: { type: String },
    paid_at: { type: Date },
    pay_method: { type: String },
    imp_uid: { type: String },

    /**
     *  1. (결제 대기) : 'ready' in IAMPORT
     *
     *  2. (결제완료) : 'paid' in IAMPORT
     *
     *  3. (결제 실패) : 'failed' in IAMPORT
     *
     *  4. (결제 취소) : 'cancelled' in IAMPORT
     * 
     *  5. (결제 패스) : 'pass' KimSimWon made. admin 회원은 결제 패스.
     * 
     *  6. (환불): 'refund'
     *
     **/
    status: {
        type: String,
        enum: ['ready', 'paid', 'failed', 'cancelled', 'pass', 'refund'], // 
        default: 'ready'
    },

    isRequestTaxBill: { type: Boolean, default: false },

    // Common Properties
    isDeleted: { type: Boolean, default: false },
    createdAt: {
        type: Date, default: function () {
            return new Date();
        }
    },
    updatedAt: {
        type: Date, default: function () {
            return new Date();
        }
    },
    expiredAt: {
        type: Date
    },

    lastUpdatedAt: {
        type: Date, default: function () {
            return new Date();
        }
    },  // 수정순으로 가져올때 사용됨

    limitDate: { type: Date },

    // Common Associations
    owner: { type: Number, ref: 'User' },
    campaign: { type: Number, ref: 'Campaign' },
    createdBy: { type: Number, ref: 'User' },
    updatedBy: { type: Number, ref: 'User' },
    description: { type: String }
});

module.exports = mongoose.model('Payment', schema);