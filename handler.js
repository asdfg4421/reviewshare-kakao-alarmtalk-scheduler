'use strict';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

mongoose.connect(process.env.MONGODB_URL);

global.User = require('./model/User');
global.Campaign = require('./model/Campaign');
global.CampaignRequest = require('./model/CampaignRequest');
global._ = require('lodash');

global.Moment = require('moment-timezone');
Moment().tz('Asia/Seoul').format();

const alarmTalkService = require('./service/AlarmTalkService');
const mailService = require('./service/MailService');
const slackService = require('./service/SlackService');

const dao = require('./dao/Dao');

const kakaoTemplates = require('./kakaotemplate/Template');

const insertDash = (number) => {
    return number.replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})/, "$1-$2-$3").replace("--", "-");
}

const setProjectChannel = (campaign) => {

    if (campaign.sns === 'NaverBlog') {
        return '네이버블로그';
    } else if (campaign.sns === 'Instagram') {
        return '인스타그램';
    } else if (campaign.sns === 'KakaoStroy') {
        return '카카오스토리';
    } else if (campaign.sns === 'Facebook') {
        return '페이스북';
    } else if (campaign.sns === 'Youtube') {
        return '유튜브';
    } else if (campaign.sns === 'NaverPost') {
        return '네이버포스트';
    } else if (campaign.sns === 'OtherSns') {
        return campaign.otherSns;
    }
}

/**
 * 리뷰어 선정안내 : 선정된 리뷰어에게 리뷰제출 시작일에 발송되는 메시지.
 */

module.exports.sendToChosenUser = async event => {

    try {

        const chosenRequestList = await dao.findChosenUser();

        const sendAlarmTalkParamsArray = [];
        const sendEmailParamsArray = [];

        if (chosenRequestList.length > 0) {

            chosenRequestList.forEach(async request => {

                const projectCategory = request.campaign.category === 'Visit' ? '방문형' : '재택형';
                const projectChannel = setProjectChannel(request.campaign);

                const guideLineUrl = `https://reviewshare.io/project/${request.campaign._id}`;
                const templateId = projectCategory === '재택형' ?
                    kakaoTemplates['chosenReviewerDelivery'] : kakaoTemplates['chosenReviewerVisit'];

                const submitItemName = request.campaign.sns === 'OtherSns' ? '리뷰를' : 'URL을';

                let message = `[리뷰쉐어]\n`
                message += `${request.owner.name}님, 축하드립니다!\n`
                message += `${request.campaign.title} 프로젝트의 리뷰어로 선정되셨습니다.\n`;
                message += `가이드라인을 참고하여 리뷰 작성 후 ${submitItemName} 제출해주세요.\n\n`;
                message += `▶ 진행방식: ${projectCategory}\n`;
                message += `▶ 진행채널: ${projectChannel}\n`;
                message += `▶ 제공내역: ${request.campaign.itemName}\n`;
                message += `▶ 제출마감: ${Moment(request.campaign.submitEndAt).format('YYYY년 MM월 DD일')}\n`;

                if (projectCategory === '재택형') { // 재택형인경우.
                    message += `▶ 담당자 연락처: ${insertDash(request.campaign.phoneNumber)}\n`;
                } else { // 방문형인 경우.
                    message += `▶ 방문주소: ${request.campaign.address.title || request.campaign.address.address_name} ${request.campaign.addressDetail || ''}\n`;
                    message += `▶ 방문일정: ${request.campaign.visitDesc}\n`;
                    message += `▶ 예약번호: ${insertDash(request.campaign.phoneNumber)}\n`;
                }

                message += `▶ 가이드라인\n${guideLineUrl}\n\n`;
                message += `※리뷰 게시기간은 최소 6개월입니다:)`;

                /************************************************************************************************
                                                        production              
                ************************************************************************************************/
                // sendAlarmTalkParamsArray.push([message, templateId, request.owner.phoneNumber]);

                /************************************************************************************************
                                                         development
                ************************************************************************************************/
                sendAlarmTalkParamsArray.push([message, templateId, '01094971722']);

                const templateData = {
                    name: request.owner.name,
                    campaignCategory: projectCategory,
                    campaignSns: projectChannel,
                    user: request.owner,
                    campaign: request.campaign,
                    campaignEndAt: Moment(request.campaign.submitEndAt).format('YYYY년 MM월 DD일'),
                    url: 'https://reviewshare.io/project/' + request.campaign._id,
                    mobileUrl: 'https://reviewshare.io/project/' + request.campaign._id
                };

                /************************************************************************************************
                                                      production              
                ************************************************************************************************/

                // sendEmailParamsArray.push([
                //     request.owner.email,
                //     projectCategory === '방문형' ? 'chosenVisit_kr.txt' : 'chosenDelivery_kr.txt',
                //     templateData
                // ]);

                /************************************************************************************************
                                                      development
                ************************************************************************************************/
                sendEmailParamsArray.push([
                    'rlatla456@naver.com',
                    projectCategory === '방문형' ? 'chosenVisit_kr.txt' : 'chosenDelivery_kr.txt',
                    templateData
                ]);
            });

            // for (const params of sendEmailParamsArray) {
            //     await mailService.sendMail(...params);
            // }

            // for (const params of sendAlarmTalkParamsArray) {
            //     await alarmTalkService.sendAlarmTalk(...params);
            // }

            await alarmTalkService.sendAlarmTalk(...sendAlarmTalkParamsArray[0]);

            return;

            // return await slackService.sendSlack(`선정된 리뷰어에게 리뷰제출 시작일에 발송되는 메시지 ::: ${chosenRequestList.length}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`선정된 리뷰어에게 리뷰제출 시작일에 발송되는 메시지 ::: ${err.message}`);
    }
};

/**
 * 리뷰 제출 마감 7일 안내 : 선정된 리뷰어중 아직 리뷰를 제출하지 않은 리뷰어들에게 제출 마감 7일전에 발송되는 메시지.
 */

module.exports.sendToUnsubmitUsers7Days = async event => {

    try {

        const unsubmitUsers = await dao.findUnsubmitUser7days();

        const templateId = kakaoTemplates['unsubmitReview7Days'];

        if (unsubmitUsers.length > 0) {

            const sendAlarmTalkParamsArray = unsubmitUsers.map(request => {

                const guideLineUrl = `https://reviewshare.io/project/${request.campaign._id}`;

                let message = "[리뷰쉐어] 제출 기한 안내(일괄전송)\n";
                message += `${request.owner.name} 리뷰어님,\n`;
                message += `${request.campaign.title} 프로젝트의 리뷰 제출 기한이 【7일】 남았습니다.\n`;
                message += "기한 내에 리뷰를 제출해주세요:)\n\n";
                message += "▶ 프로젝트 보기\n";
                message += `${guideLineUrl}`;

                /************************************************************************************************
                                                       production              
               ************************************************************************************************/
                // return [message, templateId, request.owner.phoneNumber];

                /************************************************************************************************
                                                        development
                ************************************************************************************************/
                return [message, templateId, '01094971722'];
            });

            await alarmTalkService.sendAlarmTalk(...sendAlarmTalkParamsArray[0]);

            return;

            // for (const params of sendAlarmTalkParamsArray) {
            //     await alarmTalkService.sendAlarmTalk(...params);
            // }

            // return await slackService.sendSlack(`선정된 리뷰어중 아직 리뷰를 제출하지 않은 리뷰어들에게 제출 마감 7일전에 발송되는 메시지 ::: ${unsubmitUsers.length}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`선정된 리뷰어중 아직 리뷰를 제출하지 않은 리뷰어들에게 제출 마감 7일전에 발송되는 메시지 ::: ${err.message}`);
    }
};


/**
 * 리뷰 제출 마감 1일 안내 : 선정된 리뷰어중 아직 리뷰를 제출하지 않은 리뷰어들에게 제출 마감 하루 전에 발송되는 메시지.
 */

module.exports.sendToUnsubmitUsers1Day = async event => {

    try {

        const unsubmitUsers = await dao.findUnsubmitUser1day();

        const templateId = kakaoTemplates['unsubmitReview1Day'];

        if (unsubmitUsers.length > 0) {

            const sendAlarmTalkParamsArray = unsubmitUsers.map(request => {

                const guideLineUrl = `https://reviewshare.io/project/${request.campaign._id}`;

                let message = "[리뷰쉐어] 제출 기한 안내(일괄전송)\n";
                message += `${request.owner.name} 리뷰어님,\n`;
                message += `${request.campaign.title} 프로젝트의 리뷰 제출 기한이 【하루】 남았습니다.\n\n`;
                message += "기한이 지나면 페널티 1회가 부여되며, 3회 누적 시 신규 신청이 불가합니다.\n";
                message += "서둘러 리뷰를 제출해주세요:)\n\n";
                message += "▶ 프로젝트 보기\n";
                message += `${guideLineUrl}`;

                /************************************************************************************************
                                                       production              
               ************************************************************************************************/
                // return [message, templateId, request.owner.phoneNumber];

                /************************************************************************************************
                                                        development
                ************************************************************************************************/
                return [message, templateId, '01094971722'];
            });

            await alarmTalkService.sendAlarmTalk(...sendAlarmTalkParamsArray[0]);

            return;

            // for (const params of sendAlarmTalkParamsArray) {
            //     await alarmTalkService.sendAlarmTalk(...params);
            // }

            // return await slackService.sendSlack(`선정된 리뷰어중 아직 리뷰를 제출하지 않은 리뷰어들에게 제출 마감 하루 전에 발송되는 메시지 ::: ${unsubmitUsers.length}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`선정된 리뷰어중 아직 리뷰를 제출하지 않은 리뷰어들에게 제출 마감 하루 전에 발송되는 메시지 ::: ${err.message}`);
    }
};

/**
 * 리뷰 유지 안내 : 인스타그램 리뷰를 올리고 6개월동안 유지 하지 않고 삭제한 경우 발송되는 메시지.
 */

module.exports.checkRemovedInstagramReviews = async event => {

    const rq = require('request-promise-native');

    let count = 0;

    try {

        const instagramReviews = await dao.findInstagramReviews6months();

        const templateId = kakaoTemplates['reviewRemove'];

        if (instagramReviews.length > 0) {

            await Promise.all(instagramReviews.map(async request => {

                try {

                    const option = {
                        method: 'GET',
                        uri: request.url,
                        headers: {
                            'User-Agent': ' Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36'
                        },
                        resolveWithFullResponse: true
                    };

                    await rq(option);

                } catch (err) {

                    const statusCode = err.statusCode;
                    if (statusCode === 404) { // 리뷰를 삭제한 경우.

                        let message = "[리뷰쉐어]\n";
                        message += `${request.owner.name} 리뷰어님,\n`;
                        message += `제출하신 ${request.campaign.title} 프로젝트의 리뷰가 정상 확인되지않아 연락드립니다.\n\n`;
                        message += "[마이페이지]-[내 프로젝트 활동]-[리뷰 수정하기] 버튼을 통해 다시 제출하실 수 있습니다.\n";
                        message += "리뷰 게시기간은 최소 6개월입니다:)\n\n";
                        message += "▶ 프로젝트 보기\n";
                        message += `https://reviewshare.io/project/${request.campaign._id}`;

                        /************************************************************************************************
                                                   production              
                        ************************************************************************************************/
                        // return alarmTalkService.sendAlarmTalk(message, templateId, request.owner.phoneNumber);

                        /************************************************************************************************
                                                                development
                        ************************************************************************************************/

                        count += 1;
                        await alarmTalkService.sendAlarmTalk(message, templateId, '01094971722');
                    }
                }
            }));

            // return await slackService.sendSlack(`인스타그램 리뷰를 올리고 6개월동안 유지 하지 않고 삭제한 경우 발송되는 메시지 ::: ${count}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`인스타그램 리뷰를 올리고 6개월동안 유지 하지 않고 삭제한 경우 발송되는 메시지 ::: ${err.message}`);
    }
}

/**
 * 선정일 안내 : 오늘이 프로젝트 리뷰어 선정일인 광고주에게 선정임을 알려주는 메시지.
 */

module.exports.sendToAdvertiserChoiceDay = async event => {

    try {

        const projects = await dao.findChoiceDayAdvertiser();

        const templateId = kakaoTemplates['choiceDay'];

        if (projects.length > 0) {

            const sendAlarmTalkParamsArray = projects.map(project => {

                let message = "[리뷰쉐어]\n";
                message += `${project.owner.name} 광고주님,\n오늘은 ${project.title} 프로젝트의 선정일입니다.\n`;
                message += "신청 목록을 확인하시고 원하는 리뷰어를 선정해주세요.\n\n";
                message += "▶ 선정된 리뷰어님들께는 내일 오전 10시에 선정 알림이 발송됩니다.\n\n";
                message += "▶ 리뷰어 정보(연락처, 주소 등)는 내일부터 확인하실 수 있습니다.\n";
                message += "[마이페이지-프로젝트 관리-프로젝트 선택-리뷰어 정보 다운로드]\n\n";
                message += "https://reviewshare.io";

                /************************************************************************************************
                                                         production              
                 ************************************************************************************************/
                // return [message, templateId, project.owner.phoneNumber];

                /************************************************************************************************
                                                        development
                ************************************************************************************************/
                return [message, templateId, '01094971722'];
            });

            await alarmTalkService.sendAlarmTalk(...sendAlarmTalkParamsArray[0]);

            return;

            // for (const params of sendAlarmTalkParamsArray) {
            //     await alarmTalkService.sendAlarmTalk(...params);
            // }

            // return await slackService.sendSlack(`오늘이 리뷰어 선정일인 광고주에게 선정임을 알려주는 메시지 ::: ${projects.length}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`오늘이 리뷰어 선정일인 광고주에게 선정임을 알려주는 메시지 ::: ${err.message}`);
    }
}

/**
 * 리뷰 유지 안내 : 제출 마감일이 지나고 프로젝트가 완료됨을 광고주에게 알려주는 메시지.
 */

module.exports.sendToAdvertiserProjectComplete = async event => {

    try {

        const projects = await dao.findTodayCompleteProjects();

        const templateId = kakaoTemplates['projectComplete'];

        const sendAlarmTalkParamsArray = [];
        const sendEmailParamsArray = [];

        if (projects.length > 0) {

            for (const project of projects) {

                let message = "[리뷰쉐어]\n";
                message += `${project.owner.name} 광고주님,\n`;
                message += `${project.title} 프로젝트 일정이 종료되었습니다.\n`;
                message += "제출된 리뷰를 확인해보세요:)\n";
                message += "미제출자가 있을 경우 제출을 직접 요청하실 수 있습니다.\n\n";
                message += "https://reviewshare.io";

                /************************************************************************************************
                                                        production              
                ************************************************************************************************/
                // return [message, templateId, project.owner.phoneNumber];

                /************************************************************************************************
                                                        development
                ************************************************************************************************/
                sendAlarmTalkParamsArray.push([message, templateId, '01094971722']);

                const templateData = {
                    campaign: project,
                    requestDateKr: Moment().format('YYYY년 MM월 DD일'),
                    name: project.owner.name,
                    campaignSns: setProjectChannel(project),
                    campaignCategory: project.category === 'Visit' ? '방문형' : '재택형',
                    total: project.requestUsers.length,
                    url: 'https://reviewshare.io/project/' + project._id,
                    submitted: 0,
                    left: 0
                };

                const chosenUsersCount = await dao.getChosenUsersCount(project);
                chosenUsersCount.forEach(request => {
                    if (request.isSubmitted) {
                        templateData.submitted += 1;
                    } else {
                        templateData.left += 1;
                    }
                });

                /************************************************************************************************
                                                        production              
                ************************************************************************************************/

                // sendEmailParamsArray.push([
                //     request.owner.email,
                //    'campaign-result.txt',
                //     templateData
                // ]);

                /************************************************************************************************
                                                      development
                ************************************************************************************************/
                sendEmailParamsArray.push([
                    'rlatla456@naver.com',
                    'campaign-result.txt',
                    templateData
                ]);
            };

            await alarmTalkService.sendAlarmTalk(...sendAlarmTalkParamsArray[0]);

            return;

            // for (const params of sendAlarmTalkParamsArray) {
            //     await alarmTalkService.sendAlarmTalk(...params);
            // }

            // for (const params of sendEmailParamsArray) {
            //     await mailService.sendMail(...params);
            // }

            // return await slackService.sendSlack(`제출 마감일이 지나고 프로젝트가 완료됨을 광고주에게 알려주는 메시지 ::: ${projects.length}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`제출 마감일이 지나고 프로젝트가 완료됨을 광고주에게 알려주는 메시지 ::: ${err.message}`);
    }
}

/**
 * 웰컴포인트 만료 3일전 안내 : 웰컴포인트 만료가 3일 남음을 광고주에게 알려주는 메시지.
 */

module.exports.sendToAdvertiserWelcomePoint3days = async event => {

    try {

        const advertisers = await dao.findWelcomePointExpireBefore3DaysAdvertiser();

        const templateId = kakaoTemplates['welcomePointExpire3days'];

        if (advertisers.length > 0) {

            const sendAlarmTalkParamsArray = advertisers.map(advertiser => {

                let message = "[리뷰쉐어]";
                message += `${advertiser.name} 광고주님,\n`;
                message += "리뷰어 2명을 무료로 모집할 수 있는 웰컴포인트가 3일 후 소멸됩니다.\n\n";
                message += "[리뷰어 모집하기] 메뉴에서 곧바로 모집해보세요.\n";
                message += "모집이 끝나면 신청 리스트에서 원하는 리뷰어를 고르시면 됩니다:)\n\n";
                message += "https://reviewshare.io";

                /************************************************************************************************
                                                        production              
                ************************************************************************************************/
                // return [message, templateId, null, advertiser.phoneNumber];

                /************************************************************************************************
                                                        development
                ************************************************************************************************/
                return [message, templateId, '01094971722'];
            });

            await alarmTalkService.sendAlarmTalk(...sendAlarmTalkParamsArray[0]);

            return;

            // for (const params of sendAlarmTalkParamsArray) {
            //     await alarmTalkService.sendAlarmTalk(...params);
            // }

            // return await slackService.sendSlack(`웰컴포인트 만료가 3일 남음을 광고주에게 알려주는 메시지 ::: ${advertisers.length}`);
        }

    } catch (err) {
        console.log(err);
        // return await slackService.sendSlack(`웰컴포인트 만료가 3일 남음을 광고주에게 알려주는 메시지 ::: ${err.message}`);
    }
}

module.exports.receiveSendResult = async event => {

    console.log('receive request!');
    console.log(event);

    return {
        statusCode: 200
    };
};